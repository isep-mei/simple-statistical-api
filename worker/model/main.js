var connection, host, port;

function MainHost(connection, host, port) {
    this.connection = connection;
    this.host = host;
    this.port = port;
}

MainHost.prototype.getUri = function() {
    return this.connection + "://" + this.host + ":" + this.port;
};

module.exports = MainHost;