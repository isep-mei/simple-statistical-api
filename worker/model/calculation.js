var CALCULATION_TYPE = [
    {type: "count", action: count},
    {type: "sum", action: sum},
    {type: "mean", action: mean},
    {type: "median", action: median},
    {type: "mode", action: mode},
    {type: "midrange", action: midrange},
    {type: "variance", action: variance},
    {type: "desviation", action: desviation},
];

function count(dataset, options = {}) {
    return dataset;
}

function sum(dataset, options = {}) {
    return dataset;
}

function mean(dataset, options = {}) {
    return dataset;
}

function median(dataset, options = {}) {
    return dataset;
}

function mode(dataset, options = {}) {
    return dataset;
}

function midrange(dataset, options = {}) {
    return dataset;
}

function variance(dataset, options = {}) {
    return dataset;
}

function desviation(dataset, options = {}) {
    return dataset;
}

module.exports.TYPES = CALCULATION_TYPE;