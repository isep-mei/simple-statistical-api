var actionId, dataset;

function Response(actionId, dataset) {
    this.actionId = actionId;
    this.dataset = dataset;
}

Response.prototype.getDataset = function() {
    return this.dataset;
};

Response.prototype.getEndpoint = function() {
    return "/v1/worker/" + this.actionId;
};

module.exports = Response;