var values;

function Dataset(values) {
    this.values = values;
}

Dataset.prototype.getValues = function() {
    return this.values;
};

Dataset.prototype.clone = function() {
    return new Dataset(this.values);
}

module.exports = Dataset;