var TRANSFORMATION_TYPE = [
    {type: "transpose", action: transpose},
    {type: "scale", action: scale},
    {type: "scalar", action: scalar},
    {type: "add", action: add},
    {type: "multiply", action: multiply},
    {type: "interpolation", action: interpolation},
];

function transpose(dataset, options = {}) {
    return dataset;
}

function scale(dataset, options = {}) {
    return dataset;
}

function scalar(dataset, options = {}) {
    return dataset;
}

function add(dataset, options = {}) {
    return dataset;
}

function multiply(dataset, options = {}) {
    return dataset;
}

function interpolation(dataset, options = {}) {
    return dataset;
}

module.exports.TYPES = TRANSFORMATION_TYPE;