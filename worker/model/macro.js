var calculation = require('./calculation'),
    transformation = require('./transformation'),
    chart = require('./chart');

var actions, next_action;

function Macro(actions) {
    this.actions = actions;
    this.next_action = 0;
}

Macro.prototype.getActions = function() {
    return this.actions;
};

Macro.prototype.hasAction = function() {
    return this.next_action < this.actions.length;
};

Macro.prototype.doAllActions = function(dataset) {
    while(this.hasAction()) {
        dataset = this.doNext(dataset);
    }
    return dataset;
}

Macro.prototype.doNext = function(dataset) {
    if(!this.hasAction()) { return dataset; }
    action = this.actions[this.next_action++].split("/");
    switch(action[0]) {
        case "calculate":
            return calculation.TYPES.find(c => {return c.type === action[1]}).action(dataset)
        case "transformation":
            return transformation.TYPES.find(t => {return t.type === action[1]}).action(dataset)
        case "chart":
            return chart.TYPES.find(c => {return c.type === action[1]}).action(dataset) 
        default:
            return dataset;
    }
};

Macro.prototype.clone = function() {
    return new Macro(this.actions);
}

module.exports = Macro;