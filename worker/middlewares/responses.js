
function respondIfNoError(obj, req, res, next) {
    process.app_processes--;
    if(process.app_processes == 0) { process.app_status = "ok"; }
    if(res.statusCode >= 200 && res.statusCode < 300) {
        if(res.statusCode == 202) {
            obj = createAcceptedObject(obj);
        }
        return sendAcceptTypeResponse(obj, req, res, next);
    }
    next(obj);
}

function createAcceptedObject(action_id) {
    return {
        id: action_id,
        message: "Accepted action"
    };
}

function sendAcceptTypeResponse(obj, req, res, next) {
    res.set('Content-Type', 'application/json');
    res.json(obj);
}

module.exports.respondIfNoError = respondIfNoError;
module.exports.sendAcceptTypeResponse = sendAcceptTypeResponse;