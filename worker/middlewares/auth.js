var error = require('../controllers/errors'),
    response = require('./responses');

function validateAppToken(req, res, next) {
    if(process.app_token == req.get('Authorization')) {
        next();
    } else {
        res.status(401);
        response.sendAcceptTypeResponse(error.createErrorObject(401, "Invalid token", "The authorization token is invalid."), req, res);
    }
    process.app_processes++;
    if(process.app_status === "ok") { process.app_status = "working"; }
}

module.exports.validateAppToken = validateAppToken;