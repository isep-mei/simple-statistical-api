var MainHostModel = require('../model/main'),
    request = require('../utils/request');

function getAvailable() {
    return new MainHostModel("http", "localhost", "10000");
}

function getMainUri() {
    return getAvailable().getUri();
}

function postAction(responseModel) {
    url = getMainUri() + responseModel.getEndpoint();
    headers = {
        "Authorization": process.app_token,
        "Content-Type": "application/json"
    }
    body = responseModel.getDataset()
    request.makeAsyncPost(url, headers, body, (response, body, error) => {
        if(error) { console.error("Couldn't connect to main.") }
        else {
            console.log("Async request success.");
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.
        }
    })
}

module.exports.postAction = postAction;