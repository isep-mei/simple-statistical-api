
function rootGet(req, res, next) {
    obj = {
        status: process.app_status,
        processes: process.app_processes
    }
    res.status(200);
    next(obj);
}

module.exports.rootGet = rootGet;