var model = require('../model/calculation'),
    datasetController = require('./dataset'),
    error = require('./errors');

function datasetCalculationTypeGet(req, res, next) {
    dataset = datasetController.bodyToDatasetModel(req.body);
    calculationType = model.TYPES.find((calculation) => {
        return calculation.type === req.params.calculationtype;
    });
    if (calculationType) {
        calculatedDataset = calculationType.action(dataset);
        res.status(200);
        next(calculatedDataset.getValues());
    } else {
        res.status(405);
        next(error.createErrorObject(405, "Calculation not allowed", "Don't support calculation type."));
    }
}

module.exports.datasetCalculationTypeGet = datasetCalculationTypeGet;