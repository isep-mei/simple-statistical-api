var ResponseModel = require('../model/response'),
    datasetController = require('../controllers/dataset'),
    MacroModel = require('../model/macro')
    gateway = require('../gateways/main'),
    random = require('../utils/random');

function objectToMacroModel(obj) {
    actions = obj.actions;
    return new MacroModel(actions);
}

function macroPost(req, res, next) {
    dataset = datasetController.bodyToDatasetModel(req.body);
    macro = objectToMacroModel(req.body);
    action_id = random.uuid();
    res.status(202);
    process.app_processes++; // before next to avoid more than 1 process
    next({action: action_id});

    // Post Actions
    // sleep for test purposes
    sleepSeconds = 3;
    setTimeout(() => {
        dataset = macro.doAllActions(dataset);
        gateway.postAction(new ResponseModel(action_id, dataset));
        process.app_processes--;
        if(process.app_processes == 0) { process.app_status = "ok"; }
    }, sleepSeconds * 1000);
}

module.exports.macroPost = macroPost;