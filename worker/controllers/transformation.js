var model = require('../model/transformation'),
    datasetController = require('./dataset'),
    error = require('./errors');

function datasetTransformationTypePost(req, res, next) {
    dataset = datasetController.bodyToDatasetModel(req.body);
    tranformationType = model.TYPES.find(transformation => {
        return transformation.type === req.params.transformationtype;
    });
    if (tranformationType) {
        tranformedDataset = tranformationType.action(dataset);
        res.status(200);    
        next(tranformedDataset.getValues());
    } else {
        res.status(405);
        next(error.createErrorObject(405, "Transformation not allowed", "Don't support transformation type."));
    }
}

module.exports.datasetTransformationTypePost = datasetTransformationTypePost;