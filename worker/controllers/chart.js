var model = require('../model/chart'),
    datasetController = require('./dataset'),
    error = require('./errors');

function datasetChartTypeGet(req, res, next) {
    dataset = datasetController.bodyToDatasetModel(req.body);
    chartType = model.TYPES.find((chart) => {
        return chart.type === req.params.charttype;
    });
    if (chartType) {
        chartImage = chartType.action(dataset);
        res.status(200);
        res.set('Content-Type', 'image/jpeg');
        res.send(chartImage);
        process.app_processes--;
        if(process.app_processes == 0) { process.app_status = "ok"; }
    } else {
        res.status(405);
        next(error.createErrorObject(405, "Chart not allowed", "Don't support chart type."));
    }
}

module.exports.datasetChartTypeGet = datasetChartTypeGet;