var DatasetModel = require('../model/dataset');

function bodyToObj(body) {
    data = body;
    return data;
}

function objectToDatasetModel(obj) {
    values = obj.dataset;
    return new DatasetModel(values);
}

function bodyToDatasetModel(body) {
    obj = bodyToObj(body);
    return objectToDatasetModel(obj);
}

module.exports.bodyToObj = bodyToObj;
module.exports.objectToDatasetModel = objectToDatasetModel;
module.exports.bodyToDatasetModel = bodyToDatasetModel;