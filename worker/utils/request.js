var request = require('request');

const MAX_RETRIES = 2;

var requests_made = 0;

function makeAsyncPost(url, headers, body, callback) {
    request({
        method: 'POST',
        url: url,
        headers: headers,
        json: body,
    },
    (error, response, body) => {
        if (error) {
            if(requests_made <= MAX_RETRIES) { requests_made++; makeAsyncPost(url, headers, body, callback); }
            else { requests_made = 0; callback(response, body, error); }
        } else { requests_made = 0; callback(response, body, error); }
    });
}

module.exports.makeAsyncPost = makeAsyncPost;