const uuidv4 = require('uuid/v4');

function uuid() {
    return uuidv4();
}

module.exports.uuid = uuid;