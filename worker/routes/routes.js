var express = require('express'),
    router = express.Router(),
    error = require('../controllers/errors'),
    response = require('../middlewares/responses'),
    auth = require('../middlewares/auth');

const API_VERSION = 1 | process.env.npm_package_version;

router.use('/v' + API_VERSION,
    auth.validateAppToken,
    router.use('/', require('./root')),
    router.use('/dataset', require('./dataset')),
);

router.use(response.respondIfNoError);

// catch 404, forward to error handler and format the response
router.use(error.notFound, error.handlerError, response.sendAcceptTypeResponse);

module.exports = router;