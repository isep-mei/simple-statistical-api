var express = require('express'),
    router = express.Router(),
    calculationController = require('../controllers/calculation'),
    transformationController = require('../controllers/transformation'),
    chartController = require('../controllers/chart'),
    macroController = require('../controllers/macro');

router.post('/calculate/:calculationtype', calculationController.datasetCalculationTypeGet);
router.post('/transformation/:transformationtype', transformationController.datasetTransformationTypePost);
router.post('/chart/:charttype', chartController.datasetChartTypeGet);
router.post('/macro', macroController.macroPost);

module.exports = router;