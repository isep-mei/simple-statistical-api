var request = require('../utils/request');

var workers = [];

function getAvailable() {
    return workers.find(worker => {
        return worker.isAvailable();
    });
}

function addWorker(workerModel) {
    return workers.push(workerModel);
}

function removeWorker(worker) {
    return workers.pop(worker);
}

function getWorkers() {
    return workers;
}

function getWorker(token) {
    return workers.find(worker => {
        return worker.getToken() === token;
    });
}

function postActionToAvailableWorker(endpoint, headers, body, callback) {
    worker = getAvailable();
    url = worker.getUri() + endpoint;
    headers["Authorization"] = worker.getToken();
    headers["Content-Type"] = "application/json";
    request.makePost(url, headers, body, (response, body, error) => {
        if(error) { console.error("Couldn't connect to main.") }
        else {
            if(response.statusCode < 200 && response.statusCode >= 400) {error = body}
            /*console.log("Async request success.");
            console.log('error:', error); // Print the error if one occurred
            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            console.log('body:', body); // Print the HTML for the Google homepage.*/
        }
        callback(response, body, error, worker);
    });
}

function sendCalculation(action, dataset, callback) {
    endpoint = "/v1/dataset/calculate/" + action
    headers = {}
    body = {
        dataset: dataset.getValues()
    }
    postActionToAvailableWorker(endpoint, headers, body, callback);
}

function sendTransformation(action, dataset, callback) {
    endpoint = "/v1/dataset/transformation/" + action
    headers = {}
    body = {
        dataset: dataset.getValues()
    }
    postActionToAvailableWorker(endpoint, headers, body, callback);
}

function sendChart(action, dataset, callback) {
    endpoint = "/v1/dataset/chart/" + action
    headers = {}
    body = {
        dataset: dataset.getValues()
    }
    postActionToAvailableWorker(endpoint, headers, body, callback);
}

function sendMacro(macro, dataset, callback) {
    endpoint = "/v1/dataset/macro"
    headers = {
        "Content-Type": "application/json"
    }
    body = {
        actions: macro.getActions(),
        dataset: dataset.getValues()
    }
    postActionToAvailableWorker(endpoint, headers, body, callback);
}

module.exports.getWorkers = getWorkers;
module.exports.addWorker = addWorker;
module.exports.removeWorker = removeWorker;
module.exports.sendCalculation = sendCalculation;
module.exports.sendTransformation = sendTransformation;
module.exports.sendChart = sendChart;
module.exports.sendMacro = sendMacro;