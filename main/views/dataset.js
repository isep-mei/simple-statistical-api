
function transformOne(datasetModel) {
    delete datasetModel.dbid;
    return datasetModel;
}

function transformArray(datasetArray) {
    // .map is a blocking operation function (synchronous)
    return datasetArray.map(datasetModel => {
        return transformOne(datasetModel);
    });
}

module.exports.transformOne = transformOne;
module.exports.transformArray = transformArray;