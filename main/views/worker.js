
function transformOne(workerModel) {
    workerDTO = {
        uri: workerModel.getUri(),
        status: workerModel.getStatus(),
    }
    return workerDTO;
}

function transformArray(workerArray) {
    return workerArray.map(workerModel => {
        return transformOne(workerModel);
    });
}

module.exports.transformOne = transformOne;
module.exports.transformArray = transformArray;