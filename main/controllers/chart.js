var model = require('../model/chart'),
    utils = require('./utils'),
    gateway = require('../gateways/workers'),
    error = require('./errors');

function chartsGet(req, res, next) {
    chartType = []
    model.TYPES.forEach(chart => {
        chartType.push(chart.type);
    });
    res.status(200);
    next(chartType);
}

function datasetChartsGet(req, res, next) {
    datasetUuid = req.params.datasetid;
    chartType = []
    model.TYPES.forEach(chart => {
        chartType.push("/v1/" + datasetUuid + "/chart/" + chart.type);
    });
    res.status(200);
    next(chartType);
}

function datasetChartTypeGet(req, res, next) {
    datasetUuid = req.params.datasetid;
    chartType = model.TYPES.find((chart) => {
        return chart.type === req.params.charttype;
    });
    if (chartType) {
        utils.findDataset(datasetUuid, res, next, dataset => {
            gateway.sendChart(chartType.type, dataset, (response, body, error) => {
                if(error) {
                    res.status(response.status);
                    next(createErrorObject(response.status, err.message, err.description));
                } else {
                    res.set('Content-Type', response.headers["content-type"]);
                    res.status(200);
                    res.send(body);
                }
            });
        });
    } else {
        res.status(405);
        next(error.createErrorObject(405, "Chart not allowed", "Don't support chart type."));
    }
}

module.exports.chartsGet = chartsGet;
module.exports.datasetChartsGet = datasetChartsGet;
module.exports.datasetChartTypeGet = datasetChartTypeGet;