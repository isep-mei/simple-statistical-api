//const model = require('../models/user');

function rootGet(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function rootPost(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function usernameGet(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function usernamePost(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function usernameDelete(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

module.exports.rootGet = rootGet;
module.exports.rootPost = rootPost;
module.exports.usernameGet = usernameGet;
module.exports.usernamePost = usernamePost;
module.exports.usernameDelete = usernameDelete;