var model = require('../model/calculation'),
    utils = require('./utils'),
    gateway = require('../gateways/workers'),
    DatasetModel = require('../model/dataset'),
    datasetDTO = require('../views/dataset'),
    error = require('./errors');

function calculationsGet(req, res, next) {
    calculationType = []
    model.TYPES.forEach(calculation => {
        calculationType.push(calculation.type);
    });
    res.status(200);
    next(calculationType);
}

function datasetCalculationsGet(req, res, next) {
    datasetUuid = req.params.datasetid;
    calculationType = []
    model.TYPES.forEach(calculation => {
        calculationType.push("/v1/" + datasetUuid + "/calculate/" + calculation.type);
    });
    res.status(200);
    next(calculationType);
}

function datasetCalculationTypeGet(req, res, next) {
    datasetUuid = req.params.datasetid;
    calculationType = model.TYPES.find((calculation) => {
        return calculation.type === req.params.calculationtype;
    });
    if (calculationType) {
        utils.findDataset(datasetUuid, res, next, dataset => {
            gateway.sendCalculation(calculationType.type, dataset, (response, body, error) => {
                if(error) {
                    res.status(response.status);
                    next(createErrorObject(response.status, err.message, err.description));
                } else {
                    next(datasetDTO.transformOne(new DatasetModel(null, null, null, body)));
                }
            });
        });
    } else {
        res.status(405);
        next(error.createErrorObject(405, "Calculation not allowed", "Don't support calculation type."));
    }
}

module.exports.calculationsGet = calculationsGet;
module.exports.datasetCalculationsGet = datasetCalculationsGet;
module.exports.datasetCalculationTypeGet = datasetCalculationTypeGet;