//const model = require('../models/macro');

function rootGet(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function macroGet(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function macroPut(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function macroPost(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function macroDelete(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function macroDatasetPost(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

module.exports.rootGet = rootGet;
module.exports.macroGet = macroGet;
module.exports.macroPut = macroPut;
module.exports.macroPost = macroPost;
module.exports.macroDelete = macroDelete;
module.exports.macroDatasetPost = macroDatasetPost;