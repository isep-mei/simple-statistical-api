var storage = require('../storage/dal');

function findDataset(uuid, res, next, callback) {
    dataset = storage.getDataset(uuid);
    if (dataset) {
        callback(dataset);
    } else {
        res.status(404);
        next(error.createErrorObject(404, "Not Found", "Dataset not found."));
    }
}

function writeDataset(actionResult, res, next, callback) {
    if (actionResult) {
        callback(actionResult);
    } else {
        res.status(500);
        next(error.createErrorObject(500, "Couldn't save dataset.", "Error storing the dataset."));
    }
}

function updateDataset(uuid, dataset, callback) {
    result = storage.editDataset(uuid, dataset);
    if (result) {
        callback(dataset);
    } else {
        res.status(500);
        next(error.createErrorObject(500, "Couldn't save dataset.", "Error storing the dataset."));
    }
}

module.exports.findDataset = findDataset;
module.exports.writeDataset = writeDataset;
module.exports.updateDataset = updateDataset;