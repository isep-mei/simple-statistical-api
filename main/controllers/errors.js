
function handlerError(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    err.description = err.description || err.message;
    if (res.statusCode == 200) {
        res.status(500);
        err = createErrorObject(500, err.message, err.description);
    }
    next(err);
}

function notFound(err, req, res, next) {
    if (!err) {
        res.status(404);
        err = createErrorObject(res.status, "Not Found", "Resource not found.");
    }
    next(err);
}

// 'Fields' argument should be an array of hash object where the code is an optional key
// There are 3 possible keys: 'code', 'field', 'message'
// example: 
// {
//    "code" : 123,
//    "field" : "name",
//    "message" : "Name cannot be blank"
// }
function createErrorObject(code, message, description, ...fields) {
    json = {};
    if (code) {json.code = code;}
    json.message = message;
    json.description = description;
    if (fields.length > 0) {json.errors = [];}
    for (f of fields) {
        error = {};
        if (f.code) {error.code = f.code;}
        json.field = f.field;
        json.message = f.message;
        json.errors.push(error);
    }
    return json;
}

module.exports.handlerError = handlerError;
module.exports.notFound = notFound;
module.exports.createErrorObject = createErrorObject;