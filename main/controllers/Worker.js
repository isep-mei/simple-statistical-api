var gateway = require('../gateways/workers')
    workerDTO = require('../views/worker'),
    datasetController = require('../controllers/dataset');

function rootGet(req, res, next) {
    workers = gateway.getWorkers();
    res.status(200);
    next(workerDTO.transformArray(workers));
}

function workerGet(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function workerPut(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function workerDelete(req, res, next) {
    obj = { title: 'Express' }
    next(obj);
}

function workerPostAction(req, res, next) {
    action_id = req.params.actionid;
    worker = gateway.getWorker(req.get('Authorization'));
    if(worker) {
        action = worker.popAction(action_id);
        if(action) {
            obj = {uuid: random.uuid()};
            obj.userUuid = random.uuid(); // req.Auth.user -> get current userUuid;
            obj.values = bodyToObj(req);
            dataset = objectToDatasetModel(obj);
            created = storage.addDataset(dataset);
            action.response.redirect(302, '/v1/dataset/' + uuid);
            action.response.end();
        } else {
            res.status(401);
            next(error.createErrorObject(400, "Invalid action", "The actions is not recognized."));
        }
    } else {
        res.status(401);
        next(error.createErrorObject(401, "Invalid token", "The authorization token is invalid."));
    }
}

module.exports.rootGet = rootGet;
module.exports.workerGet = workerGet;
module.exports.workerPut = workerPut;
module.exports.workerDelete = workerDelete;
module.exports.workerPostAction = workerPostAction;