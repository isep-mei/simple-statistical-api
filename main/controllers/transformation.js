var model = require('../model/transformation'),
    utils = require('./utils'),
    gateway = require('../gateways/workers'),
    error = require('./errors');

function transformationsGet(req, res, next) {
    transformationType = []
    model.TYPES.forEach(transformation => {
        transformationType.push(transformation.type);
    });
    res.status(200);
    next(transformationType);
}

function datasetTransformationsGet(req, res, next) {
    datasetUuid = req.params.datasetid;
    transformationType = []
    model.TYPES.forEach(transformation => {
        transformationType.push("/v1/" + datasetUuid + "/transformation/" + transformation.type);
    });
    res.status(200);
    next(transformationType);
}

function datasetTransformationTypePost(req, res, next) {
    datasetUuid = req.params.datasetid;
    transformationType = model.TYPES.find((transformation) => {
        return transformation.type === req.params.transformationtype;
    });
    if (transformationType) {
        utils.findDataset(datasetUuid, res, next, dataset => {
            gateway.sendTransformation(transformationType.type, dataset, (response, body, error) => {
                if(error) {
                    res.status(response.status);
                    next(createErrorObject(response.status, err.message, err.description));
                } else {
                    dataset.setValues(body);
                    utils.updateDataset(dataset.getUuid(), dataset, dataset => {
                        res.redirect(302, '/v1/dataset/'+dataset.getUuid());
                        res.end();
                    })
                }
            });
        });
    } else {
        res.status(405);
        next(error.createErrorObject(405, "Transformation not allowed", "Don't support transformation type."));
    }
}

module.exports.transformationsGet = transformationsGet;
module.exports.datasetTransformationsGet = datasetTransformationsGet;
module.exports.datasetTransformationTypePost = datasetTransformationTypePost;