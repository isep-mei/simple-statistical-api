var macros = [];

function getAllMacros() {}
function getMacrosOfUser(userUuid) {}
function getMacro(macroUuid) {}
function addMacro(macroModel) {}
function editMacro(macroUuid, macroModel) {}
function removeMacro(macroUuid) {}

module.exports.getAllMacros = getAllMacros;
module.exports.getMacrosOfUser = getMacrosOfUser;
module.exports.getMacro = getMacro;
module.exports.addMacro = addMacro;
module.exports.editMacro = editMacro;
module.exports.removeMacro = removeMacro;