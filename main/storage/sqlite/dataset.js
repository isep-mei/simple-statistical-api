var sqlite3 = require('sqlite3').verbose(),
    db = new sqlite3.Database(':memory:');
 
const TABLE_NAME = '';

function getAllDatasets() {
    db.serialize(function() {
        db.each("SELECT * FROM " + TABLE_NAME, function(err, row) {
            console.log(row.id + ": " + row.info);
        });
    });
    db.close();
}

function getDatasetsOfUser(userUuid) {
    return data.getDatasets().filter(dataset => dataset.userUuid == userUuid);
}

function getDataset(datasetUuid) {
    return data.getDatasets().find(dataset => dataset.uuid == datasetUuid);
}

function addDataset(datasetModel) {
    db.serialize(function() {
        var stmt = db.prepare("INSERT INTO " + TABLE_NAME + " VALUES (?)");
        stmt.run(datasetModel);
        stmt.finalize();
    });
    db.close();
}

function editDataset(datasetUuid, datasetModel) {
}

function removeDataset(datasetUuid) {
}

module.exports.getAllDatasets = getAllDatasets;
module.exports.getDatasetsOfUser = getDatasetsOfUser;
module.exports.getDataset = getDataset;
module.exports.addDataset = addDataset;
module.exports.editDataset = editDataset;
module.exports.removeDataset = removeDataset;