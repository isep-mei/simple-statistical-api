var dataset = require('./dataset-seed');

function seed() {
    dataset.run();
}

module.exports.seed = seed;