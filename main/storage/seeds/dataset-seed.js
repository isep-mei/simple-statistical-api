var DatasetModel = require('../../model/dataset'),
    storage = require('../dal'),
    random = require('../../utils/random');

const NUM_DATASETS_TO_CREATE = 3,
    DATASET_MAX_COLUMNS = 10,
    DATASET_MAX_ROWS = DATASET_MAX_COLUMNS,
    DATASET_MIN_VALUE = -30,
    DATASET_MAX_VALUE = 30;

function create(users_list) {
    datasets = [];
    for(i = 0; i < NUM_DATASETS_TO_CREATE; i++) {
        dbid = null;
        uuid = random.uuid();
        userUuid = users_list[random.int(0, users_list.length - 1)].uuid;
        MAX_COLUMNS = random.int(1, DATASET_MAX_COLUMNS);
        MAX_ROWS = random.int(1, DATASET_MAX_ROWS);
        values = [];
        for(column_num = 0; column_num < MAX_COLUMNS; column_num++) {
            values[column_num] = [];
            for(row_num = 0; row_num < MAX_ROWS; row_num++) {
                values[column_num].push(random.int(DATASET_MIN_VALUE, DATASET_MAX_COLUMNS));
            }
        }
        datasets.push(new DatasetModel(dbid, uuid, userUuid, values));
    }
    return datasets;
}

function store(datasets) {
    datasets.forEach(dataset => {
        storage.addDataset(dataset);
    });
}

function run() {
    users_list = [
        {"uuid":random.uuid()},
        {"uuid":random.uuid()},
        {"uuid":random.uuid()},
    ];
    datasets = create(users_list);
    store(datasets);
}

module.exports.run = run;