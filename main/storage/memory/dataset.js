var datasets = [];

function getAllDatasets() {data = []; datasets.forEach(dataset => {data.push(dataset.clone())}); return data;} // Create new structure to prevent from returning the same object
function getDatasetsOfUser(userUuid) {return getAllDatasets().filter(dataset => {return dataset.userUuid == userUuid});}
function getDataset(datasetUuid) {return getAllDatasets().find(dataset => {return dataset.uuid === datasetUuid});}
function addDataset(datasetModel) {return datasets.push(datasetModel);}
function editDataset(datasetUuid, datasetModel) {datasetToModify = datasets.find(dataset => {return dataset.uuid === datasetUuid}); if(datasetToModify != null) {datasetToModify.values = datasetModel.getValues(); return true} else {return false;};}
function removeDataset(datasetUuid) {return datasets.pop(getDataset(datasetUuid));}

var macros = [];

function getAllMacros() {data = []; macros.forEach(macro => {data.push(macro.clone())}); return data;} // Create new structure to prevent from returning the same object
function getMacrosOfUser(userUuid) {return getAllMacros().filter(macro => {return macro.userUuid === userUuid});}
function getMacro(macroUuid) {return getAllMacros().find(macro => {return macro.uuid == macroUuid});}
function addMacro(macroModel) {return macros.push(macroModel);}
function editMacro(macroUuid, macroModel) {macroToModify = macros.find(macro => {returnmacro.uuid === macroUuid}); if(macroToModify != null) {macroToModify.values = macroModel.getValues(); return true} else {return false;};}
function removeMacro(macroUuid) {return macros.pop(getMacro(macroUuid));}

module.exports.getAllMacros = getAllMacros;
module.exports.getMacrosOfUser = getMacrosOfUser;
module.exports.getMacro = getMacro;
module.exports.addMacro = addMacro;
module.exports.editMacro = editMacro;
module.exports.removeMacro = removeMacro;

module.exports.getAllDatasets = getAllDatasets;
module.exports.getDatasetsOfUser = getDatasetsOfUser;
module.exports.getDataset = getDataset;
module.exports.addDataset = addDataset;
module.exports.editDataset = editDataset;
module.exports.removeDataset = removeDataset;