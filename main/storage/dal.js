var glob = require("glob"),
    path = require('path'),
    DB_PATH = './storage',
    DB_TYPE = process.env.DATABASE_ID || 'memory';

database = glob.sync(path.join(DB_PATH + '/' + DB_TYPE, '*.js')).reduce(function (loaded, file) {
    return require('../' + file);
}, {});

module.exports = database;