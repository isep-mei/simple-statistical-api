# Simple Statistical API

## Overview
This is a Simple Statistical API.

## Running the server
To run the server: `npm start`
To run tests: `npm test` [NOT IMPLEMENTED YET]
To simulate live with seeds: `npm run-script start-seed`
To run in development: `npm run-script dev`
To run in development with seeds: `npm run-script dev-seed`
