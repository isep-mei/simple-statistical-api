var express = require('express'),
    router = express.Router(),
    controller = require('../controllers/root');

router.get('/', controller.rootGet);

module.exports = router;