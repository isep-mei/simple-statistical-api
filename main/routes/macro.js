var express = require('express'),
    router = express.Router(),
    controller = require('../controllers/macro');

router.get('/', controller.rootGet);
router.get('/:macroid', controller.macroGet);
router.put('/:macroid', controller.macroPut);
router.post('/:macroid', controller.macroPost);
router.delete('/:macroid', controller.macroDelete);
router.post('/:macroid/dataset/:datasetid', controller.macroDatasetPost);

module.exports = router;