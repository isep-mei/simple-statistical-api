var express = require('express'),
    router = express.Router(),
    controller = require('../controllers/worker');

router.get('/', controller.rootGet);
router.get('/:workerid', controller.workerGet);
router.put('/:workerid', controller.workerPut);
router.delete('/:workerid', controller.workerDelete);
router.post('/:actionid', controller.workerPostAction);

module.exports = router;