var express = require('express'),
    router = express.Router(),
    error = require('../controllers/errors'),
    response = require('../middlewares/responses');

const API_VERSION = 1 | process.env.npm_package_version;

router.use('/v' + API_VERSION,
    router.use('/', require('./root')),
    router.use('/user', require('./user')),
    router.use('/dataset', require('./dataset')),
    router.use('/macro', require('./macro')),
    router.use('/worker', require('./worker')),
);

router.use(response.respondIfNoError);

// catch 404, forward to error handler and format the response
router.use(error.notFound, error.handlerError, response.sendAcceptTypeResponse);

module.exports = router;