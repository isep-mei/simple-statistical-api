var express = require('express'),
    router = express.Router(),
    datasetController = require('../controllers/dataset'),
    calculationController = require('../controllers/calculation'),
    transformationController = require('../controllers/transformation'),
    chartController = require('../controllers/chart');

router.get('/', datasetController.rootGet);
router.post('/', datasetController.rootPost);

router.get('/calculate', calculationController.calculationsGet);
router.get('/transformation', transformationController.transformationsGet);
router.get('/chart', chartController.chartsGet);

router.get('/:datasetid', datasetController.datasetGet);
router.put('/:datasetid', datasetController.datasetPut);
router.delete('/:datasetid', datasetController.datasetDelete);

router.get('/:datasetid/calculate', calculationController.datasetCalculationsGet);
router.get('/:datasetid/transformation', transformationController.datasetTransformationsGet);
router.get('/:datasetid/chart', chartController.datasetChartsGet);

router.get('/:datasetid/calculate/:calculationtype', calculationController.datasetCalculationTypeGet);
router.post('/:datasetid/transformation/:transformationtype', transformationController.datasetTransformationTypePost);
router.get('/:datasetid/chart/:charttype', chartController.datasetChartTypeGet);

router.post('/:datasetid/macro', datasetController.datasetMacroPost);

module.exports = router;