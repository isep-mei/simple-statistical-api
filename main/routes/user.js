var express = require('express'),
    router = express.Router(),
    controller = require('../controllers/user');

router.get('/', controller.rootGet);
router.post('/', controller.rootPost);
router.get('/:username', controller.usernameGet);
router.post('/:username', controller.usernamePost);
router.delete('/:username', controller.usernameDelete);

module.exports = router;