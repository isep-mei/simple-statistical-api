const uuidv4 = require('uuid/v4');

function int(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function uuid() {
    return uuidv4();
}

module.exports.int = int;
module.exports.uuid = uuid;