var CHART_TYPE = [
    {type: "pie"},
    {type: "line"},
    {type: "bar"},
];

module.exports.TYPES = CHART_TYPE;