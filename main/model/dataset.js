var dbid, uuid, userUuid, values;

function Dataset(dbid, uuid, userUuid, values) {
    this.dbid = dbid;
    this.uuid = uuid;
    this.userUuid = userUuid;
    this.values = values;
}

Dataset.prototype.getDbId = function() {
    return this.dbid;
};

Dataset.prototype.getUuid = function() {
    return this.uuid;
};

Dataset.prototype.getUserUuid = function() {
    return this.userUuid;
};

Dataset.prototype.getValues = function() {
    return this.values;
};

Dataset.prototype.setUserUuid = function(userUuid) {
    return this.userUuid = userUuid;
};

Dataset.prototype.setValues = function(values) {
    return this.values = values;
};

Dataset.prototype.sameDbId = function(value) {
    return this.dbid === this.value;
};

Dataset.prototype.sameUuid = function(value) {
    return this.uuid === this.value;
};

Dataset.prototype.clone = function() {
    return new Dataset(this.dbid, this.uuid, this.userUuid, this.values);
}

module.exports = Dataset;