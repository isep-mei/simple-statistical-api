var CALCULATION_TYPE = [
    {type: "count"},
    {type: "sum"},
    {type: "mean"},
    {type: "median"},
    {type: "mode"},
    {type: "midrange"},
    {type: "variance"},
    {type: "desviation"},
];

module.exports.TYPES = CALCULATION_TYPE;