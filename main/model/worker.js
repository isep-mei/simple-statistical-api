var token, connection, host, port, status, actions;

function Worker(host, port, status) {
    this.token = 'xxx' || random.uuid();
    this.connection = "http";
    this.host = host || null;
    this.port = port || null;
    this.status = status || null;
    this.actions = [];
}

Worker.prototype.getToken = function() {
    return this.token;
}

Worker.prototype.getHost = function() {
    return this.host;
}

Worker.prototype.setHost = function(host){
    this.host = host;
}

Worker.prototype.getPort = function() {
    return this.port;
}

Worker.prototype.setPort = function(port) {
    this.port = port;
}

Worker.prototype.getStatus = function() {
    return this.status;
}

Worker.prototype.setStatus = function(status) {
    this.status = status;
}

Worker.prototype.getUri = function() {
    return this.connection + "://" + this.host + ":" + this.port;
}

Worker.prototype.addAction = function(id, req, res, next) {
    return this.actions.push({
        id: id,
        request: req,
        response: res,
        next: next,
    });
}

Worker.prototype.popAction = function(id) {
    action = this.actions.find(a => {
        return a.id === id;
    });
    return this.actions.pop(action);
}

Worker.prototype.isAvailable = function() {
    return this.getStatus() === "ok";
}

Worker.prototype.equals = function(otherWorker) {
    return otherWorker.getToken() === this.getToken()
        && otherWorker.getHost() === this.getHost()
        && otherWorker.getPort() == this.getPort();
}

module.exports = Worker;
