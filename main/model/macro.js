var dbid, uuid, userUuid, actions;

function Macro(dbid, uuid, userUuid, actions) {
    this.dbid = dbid;
    this.uuid = uuid;
    this.userUuid = userUuid;
    this.actions = actions;
}

Macro.prototype.getDbId = function() {
    return this.dbid;
};

Macro.prototype.getUuid = function() {
    return this.uuid;
};

Macro.prototype.getUserUuid = function() {
    return this.userUuid;
};

Macro.prototype.getActions = function() {
    return this.actions;
};

Macro.prototype.setUserUuid = function(userUuid) {
    return this.userUuid = userUuid;
};

Macro.prototype.setActions = function(actions) {
    return this.actions = actions;
};

Macro.prototype.sameDbId = function(value) {
    return this.dbid === this.value;
};

Macro.prototype.sameUuid = function(value) {
    return this.uuid === this.value;
};

Macro.prototype.clone = function() {
    return new Macro(this.dbid, this.uuid, this.userUuid, this.actions);
}

module.exports = Macro;