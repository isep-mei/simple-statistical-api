var TRANSFORMATION_TYPE = [
    {type: "transpose"},
    {type: "scale"},
    {type: "scalar"},
    {type: "add"},
    {type: "multiply"},
    {type: "interpolation"},
];

module.exports.TYPES = TRANSFORMATION_TYPE;