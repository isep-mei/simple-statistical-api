var xml = require('xml');

function respondIfNoError(obj, req, res, next) {
    if(res.statusCode >= 200 && res.statusCode < 300) {
        return sendAcceptTypeResponse(obj, req, res, next);
    }
    next(obj);
}

function sendAcceptTypeResponse(obj, req, res, next) {
    switch(req.accepts()[0]) {
        case 'application/xml':
            res.set('Content-Type', 'application/xml');
            res.send(xml(obj));
            break;
        case 'application/json':
        default:
            res.set('Content-Type', 'application/json');
            res.json(obj);
            break;
    }
}

module.exports.respondIfNoError = respondIfNoError;
module.exports.sendAcceptTypeResponse = sendAcceptTypeResponse;